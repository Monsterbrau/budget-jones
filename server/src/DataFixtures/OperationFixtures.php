<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;

class OperationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 21; $i++) {
            $operation = new Operation();
            if ((mt_rand(10, 100)) >= 50) {
                $operation->setSum(mt_rand(10, 100));
                $operation->setDescription("-positive-");
            } else {
                $operation->setSum(-(mt_rand(10, 100)));
                $operation->setDescription("-negative-");
            }
            $operation->setDate("1539266853");
            $operation->setType("cash");
            $operation->setTags(array("tag1", "tag2"));

            $manager->persist($operation);
        }
        $manager->flush();
    }
}
