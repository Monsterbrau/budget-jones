<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use App\Entity\Tag;
use App\Repository\TagRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;




/**
 * @Route("/api/tag", name="api_tag")
 */

class TagController extends Controller
{

    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }
    /**
     * @Route("", name="tag", methods={"GET"})
     */
    public function all(TagRepository $repo)
    {
        $list = $repo->findAll();
        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'operations']]);
        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("", name="new_tag", methods={"POST"})
     */
    public function addTag(Request $request) : Response
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $content = $request->getContent();
        $tag = $this->serializer->deserialize($content, Tag::class, "json");

        $manager->persist($tag);
        $manager->flush();

        $data = $this->serializer->normalize($tag, null, ['attributes' => ['id', 'name']]);

        $response = new Response($this->serializer->serialize($tag, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="delete_tag", methods={"DELETE"})
     */
    public function delete(Tag $tag)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($tag);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("/{id}", name="update_tag", methods={"PUT"})
     */
    public function update(Request $request, Tag $tag)
    {

        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Tag::class, "json");

        $budget->setName($update->getName());

        $manager->persist($tag);
        $manager->flush();

        $data = $this->serializer->normalize($tag, null, ['attributes' => ['id', 'name']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }
}
