<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\OperationRepository;
use App\Repository\BudgetRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Operation;
use App\Entity\Budget;
use App\Repository\UserRepository;
use App\Repository\TagRepository;
use App\Entity\Tag;

/**
 * @Route("/api", name="api_operation")
 */

class OperationController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/operation", name="operation", methods={"GET"})
     */
    public function all(UserRepository $repo)
    {
        $user = $this->getUser();
        $list = $repo->getOperationsByUser($user);
        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags' => ['id', 'name'], 'type', 'budgets', 'total', 'totalInput', 'totalOutput']]);
        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("/total", name="total", methods={"GET"})
     */
    public function getTotal(OperationRepository $repo)
    {
        $user = $this->getUser();
        $data = $repo->getTotal($user);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/total-input", name="total-input", methods={"GET"})
     */
    public function getTotalInput(OperationRepository $repo)
    {
        $user = $this->getUser();
        $data = $repo->getTotalInput($user);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/total-output", name="total-total", methods={"GET"})
     */
    public function getTotalOutput(OperationRepository $repo)
    {
        $user = $this->getUser();
        $data = $repo->getTotalOutput($user);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/operation", name="new_operation", methods={"POST"})
     */

    public function addOperation(Request $request, BudgetRepository $repo, TagRepository $tagRepo)
    {
        $manager = $this->getDoctrine()->getManager();
        $content = $request->getContent();
        $body = json_decode($content, true);
        $user = $this->getUser();
        $newOperation = new Operation();

        foreach ($body['tags'] as $tag) {
            $newTag = new Tag();
            $newTag->setName($tag);
            $newOperation->addTag($newTag);
        };
        foreach ($body['budgets'] as $budget) {
            $newOperation->addBudget($repo->find($budget['id']));
        }
        $newOperation->setSum($body['sum']);
        $newOperation->setDescription($body['description']);
        $newOperation->setType($body['type']);
        $newOperation->setDate($body['date']);
        $newOperation->setUser($user);
        $manager->persist($newOperation);
        $manager->flush();

        $data = $this->serializer->normalize($newOperation, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags', 'type', 'budgets', 'user']]);

        $response = new Response($this->serializer->serialize($newOperation, "json"));
        return $response;
    }

    /**
     * @Route("/operation/{id}", name="delete_operation", methods={"DELETE"})
     */
    public function delete(Operation $operation)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($operation);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("/operation/{id}", name="update_operation", methods={"PUT"})
     */
    public function update(Request $request, Operation $operation, BudgetRepository $repo)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $body = json_decode($content, true);

        foreach ($body['budgets'] as $budget) {
            $operation->addBudget($repo->find($budget['id']));
        }
        $operation->setSum($body['sum']);
        $operation->setDescription($body['description']);
        $operation->setTags($body['tags']);
        $operation->setType($body['type']);

        $manager->persist($operation);
        $manager->flush();

        $data = $this->serializer->normalize($operation, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags', 'type', 'budgets', 'user']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }
}
