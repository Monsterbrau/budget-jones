import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from '../service/authentication/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  connected: boolean = false;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {
    this.connected = this.auth.isLoggedIn();
     
  }

  logout() {
    this.auth.logout();

    this.ngOnInit();
  }
}
