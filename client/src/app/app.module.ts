import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { OperationFormComponent } from './operation-form/operation-form.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { BudgetComponent } from './budget/budget.component';
import { BudgetCardComponent } from './budget-card/budget-card.component';
import { LoginComponent } from './login/login.component';
import { AuthorizationInterceptor } from './service/interceptor/authorization.service';
import { UserFormComponent } from './user-form/user-form.component';
import { TagsComponent } from './tags/tags.component';
import { TagsMenuComponent } from './tags-menu/tags-menu.component';

library.add(fas); // Font-Awesome

const appRoutes: Routes = [
  { path: "", component: OperationFormComponent},
  { path: "update-form", component: UpdateFormComponent},
  { path: "budgets", component: BudgetComponent},
  { path: "budgets/:id", component: BudgetCardComponent},
  { path: "subscribe", component: UserFormComponent},
  { path: "tagsMenu", component: TagsMenuComponent},
  { path: "tags/:tag", component: TagsComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    OperationFormComponent,
    UpdateFormComponent,
    NavbarComponent,
    FooterComponent,
    BudgetComponent,
    BudgetCardComponent,
    UserFormComponent,
    LoginComponent,
    TagsComponent,
    TagsMenuComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
