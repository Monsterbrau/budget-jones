import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from '../service/user.service';
import { User } from '../entity/user';
import { AuthenticationService } from '../service/authentication/authentication.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  userForm: FormGroup;
  user: User = { email: null, password: null };

  constructor(private formBuilder: FormBuilder, private userServ: UserService, private authentication: AuthenticationService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, this.passwordValid]],
      confirmPassword: ["", Validators.required]
    }, {
        validator: this.matchPassword
      });

  }

  onSubmit() {

    /*mdp: test1234/ */
    this.user = this.userForm.value;
    this.userServ.add(this.user).subscribe();

  }

  passwordValid(control: FormControl) {
    /* 
    Regular expression for Password Validator: 
    ^.*              : Start
  (?=.{6,30})        : Length
  (?=.*[a-zA-Z])   : Letters
  (?=.*\d)         : Digits
  (?=.*[!#$%&? "]) : Special characters
  .*$              : End 
    */
    const isPasswordValid = /^.*(?=.{6,30})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&?/*"]).*$/i.test(control.value)
    if (!isPasswordValid) {
      return { 'invalidPassword': true }
    } else {
      return false
    }
  }

  matchPassword(group: FormGroup) {
    let password = group.get('password').value;
    let confirmPassword = group.get('confirmPassword').value;
    if (password !== confirmPassword) {
      group.get('confirmPassword').setErrors({ MatchPassword: true })
    } else {
      return null;
    }

  }

}
