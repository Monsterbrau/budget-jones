import { Budget } from "./budget";

export interface Operation {
  id?: number;
  description: string;
  sum: number;
  date: any;
  budgets?: Array<Budget>;
  tags?: Array<string>;
  type?: string;
}
