import { Budget } from "./budget";

export interface User {
  id?:number;
  email:string;
  password:string;
  budgets?:Array<Budget>;
}
