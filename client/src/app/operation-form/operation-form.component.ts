import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { OperationService } from '../service/operation.service';
import { Budget } from '../entity/budget';
import { log } from 'util';
import { BudgetService } from '../service/budget.service';
import { AuthenticationService } from '../service/authentication/authentication.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-operation-form',
  templateUrl: './operation-form.component.html',
  styleUrls: ['./operation-form.component.css']
})
export class OperationFormComponent implements OnInit {

  newOperation: Operation = { sum: null, description: null, date: null, budgets: [], tags: [], type: null };
  operations: Operation[] = [];
  total: number;
  input: number;
  output: number;
  selected: Operation = { id: 0, sum: 0, description: null, date: null, tags: [], budgets: [], type: null };
  tags: string = null;
  budgets: Budget[];
  newBudgetOperation: Budget[] = [];
  connected: boolean = false;

  constructor(private service: OperationService, private budgetService: BudgetService, private authentication: AuthenticationService) {
  }

  ngOnInit() {
    if (this.authentication.isLoggedIn()) {
      this.connected = true;
      this.service.findAll().subscribe(response => this.operations = response.reverse());
      this.service.getTotal().subscribe(response => this.total = response.total);
      this.service.getTotalInput().subscribe(response => this.input = response.totalInput);
      this.service.getTotalOutput().subscribe(response => this.output = response.totalOutput);
      this.budgetService.findAll().subscribe(response => this.budgets = response);
    }


  }

  addOperation() {
    this.newOperation.tags = this.tags.match(/\w+/g);

    this.service.add(this.newOperation).subscribe((response) => {
      this.operations.push(response);
      this.newOperation.description = null;
      this.newOperation.sum = null;
      this.ngOnInit();
    })
  }

  deleteOperation(newOperation: Operation) {
    this.service.delete(newOperation.id).subscribe((response) => {
      this.operations = this.operations.filter(currentOperation => currentOperation.id !== newOperation.id);
      this.ngOnInit();
    })
  }

  updateOperation() {
    this.service.update(this.selected).subscribe(() => {
      this.selected.id = null;
      this.ngOnInit();
    })
  }

  addBudget(item: Budget) {
    this.newOperation.budgets.push(item);
  }


}

