import { Component, OnInit } from '@angular/core';
import { BudgetService } from '../service/budget.service';
import { Budget } from '../entity/budget';

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent implements OnInit {

  budgets: Budget[];
  newBudget:Budget = {name:"",sum:null};
  addButton: boolean = false;

  constructor(private service: BudgetService) {
  }


  ngOnInit() {
    this.service.findAll().subscribe((response) => {
      this.budgets = response;
    });
  }

  onClick(){
    this.service.add(this.newBudget).subscribe(()=>this.ngOnInit());
    
  }
}
