import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from "rxjs/operators";
import * as jwtdecode from "jwt-decode";


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

 private url = "http://localhost:8080/api/login_check"
  user = new BehaviorSubject<any>(null);

  constructor(private http:HttpClient) {
    if(localStorage.getItem('token')) {
      this.user.next(
        jwtdecode(localStorage.getItem('token'))
        );
    }

   }

   login(username:string, password:string): Observable<any> {
    return this.http.post<any>(this.url, {
      username:username, 
      password:password
    }).pipe(
      tap(response => {
        if(response.token) {     
          localStorage.setItem('token', response.token);
          this.user.next(jwtdecode(response.token));
        }
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }

  isLoggedIn():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
}
