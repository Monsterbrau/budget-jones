import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService) { }

  intercept(req: HttpRequest<any>,
    next: HttpHandler): Observable<any> {

    let token = localStorage.getItem('token');
    if (token) {
      let modifiedRequest = req.clone({

        setHeaders: {
          Authorization: 'Bearer ' + token
        }

      });

      return next.handle(modifiedRequest)
        .pipe(
          catchError((err, caught) => {
            if (err.status === 401) {
              this.auth.logout();
            }
            return throwError(err);
          })
        );
    }
    return next.handle(req);
  }

}
