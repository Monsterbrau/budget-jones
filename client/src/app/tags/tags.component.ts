import { Component, OnInit } from '@angular/core';
import { OperationService } from '../service/operation.service';
import { Operation } from '../entity/operation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  operations: Operation[];
  operationFilter: Operation[] = [];
  totalInput: number = null;
  totalOutput: number = null;
  total: number = null;
  tagUrl: string = "";

  constructor(private service: OperationService, private route: ActivatedRoute) { route.params.subscribe(data => this.tagUrl = data['tag']) }

  ngOnInit() {
    this.service.findAll().subscribe((data) => {
      this.operations = data;

      for (const op of this.operations) {
        for (const tag of op.tags) {
          if (tag === this.tagUrl) {
            this.operationFilter.push(op);
          };
        }
      };

      for (const operation of this.operationFilter) {
        if (operation.sum > 0) {
          this.totalInput += operation.sum;
        } else {
          this.totalOutput += operation.sum;
        }
      };
      this.total = this.totalInput + this.totalOutput;
    });
    console.log(this.operationFilter);

  }

}