import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Operation } from '../entity/operation';
import { Budget } from '../entity/budget';
import { OperationService } from '../service/operation.service';
import { BudgetService } from '../service/budget.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  @Input() updated: Operation;
  @Input() tagsUpdate: string;
  @Input() budgets: Budget[];
  @Output() submit = new EventEmitter<any>();

  updateBudgets: Budget[] = [];






  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    this.budgetService.findAll().subscribe(data => {
      this.budgets = data;
    })
  }

  onClick() {
    this.updated.tags = this.tagsUpdate.match(/\w+/g);
    this.updated.budgets = this.updateBudgets;
    this.submit.emit(this.updated);
  }

  changeBudget(item: Budget) {

    for (const budget of this.budgets) {
      if (item.id !== budget.id) {
        this.updateBudgets.push(item);
        break
      }
    }
  }
}
