import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagsMenuComponent } from './tags-menu.component';

describe('TagsMenuComponent', () => {
  let component: TagsMenuComponent;
  let fixture: ComponentFixture<TagsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
